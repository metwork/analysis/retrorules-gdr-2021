import os
import pytest
import pandas as pd
from retrorules_gdr_2021.config import config, DATA_DIR, CFM_ID_PATH
from retrorules_gdr_2021.frag import Fragmenter


@pytest.fixture(autouse=True)
def set_envs(monkeypatch, shared_datadir):
    monkeypatch.setenv(CFM_ID_PATH, str(shared_datadir / "cfm_id"))
    monkeypatch.setenv(DATA_DIR, str(shared_datadir / "computed_data"))


def test_fragmenter_settings(shared_datadir):
    cfmid_path = str(shared_datadir / "cfm_id")
    assert os.getenv(CFM_ID_PATH) == cfmid_path
    assert config.get(CFM_ID_PATH) == cfmid_path


def test_frag_smiles():
    data = {
        "product_smiles": [
            "[H]C1(OC2C(O)C(O)C(CO)OC2OC2=C(O)C3=C(C(O)=C2)C(=O)C=C(O3)C2=CC=C(O)C(O)=C2)OC(COC(C)=O)C(O)C(O)C1O"
        ],
        "product_inchikeys": ["HLGKHCVGTSTFAJ-UHFFFAOYSA-N"],
    }
    df = pd.DataFrame(data)
    fragmenter = Fragmenter(df)
    fragmenter.gen_all_frag()
