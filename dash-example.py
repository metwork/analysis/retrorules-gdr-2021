import json
from pathlib import Path
import dash
from dash.dependencies import Input, Output
from dash.exceptions import PreventUpdate
import dash_html_components as html
import dash_cytoscape as cyto

cyto.load_extra_layouts()

elements = json.loads(Path("notebooks", "data", "network3.json").read_text())

app = dash.Dash(__name__)

styles = {
    "output": {
        "overflow-y": "scroll",
        "overflow-wrap": "break-word",
        "height": "calc(100% - 25px)",
        "border": "thin lightgrey solid",
    },
    "tab": {"height": "calc(98vh - 115px)"},
}

app.layout = html.Div(
    [
        html.Div(
            children=[
                html.Div("Download graph:"),
                html.Button("as jpg", id="btn-get-jpg"),
                html.Button("as png", id="btn-get-png"),
                html.Button("as svg", id="btn-get-svg"),
            ],
        ),
        html.Div(
            children=[
                cyto.Cytoscape(
                    id="cytoscape-graph",
                    layout={"name": "preset"},
                    style={"width": "100%", "height": "800px"},
                    stylesheet=[
                        {
                            "selector": "edge",
                            "style": {
                                # "label": "data(label)",
                                "line-color": "#073877",
                                "curve-style": "bezier",
                                # "target-arrow-color": "black",
                                # "target-arrow-shape": "triangle",
                                "opacity": "data(opacity)",
                            },
                        },
                        {
                            "selector": "node[!parent]",
                            "style": {"content": "data(label)"},
                        },
                        {
                            "selector": "node[parent]",
                            "style": {
                                # "content": "data(cosine)",
                                "background-color": "data(color)",
                            },
                        },
                    ],
                    **elements,
                )
            ],
        ),
    ]
)


@app.callback(
    Output("cytoscape-graph", "generateImage"),
    [
        Input("btn-get-jpg", "n_clicks"),
        Input("btn-get-png", "n_clicks"),
        Input("btn-get-svg", "n_clicks"),
    ],
)
def get_image(get_jpg_clicks, get_png_clicks, get_svg_clicks):

    ctx = dash.callback_context
    if ctx.triggered:
        input_id = ctx.triggered[0]["prop_id"].split(".")[0]

        if input_id != "tabs":
            ftype = input_id.split("-")[-1]

        return {"type": ftype, "action": "download"}

    raise PreventUpdate


if __name__ == "__main__":
    app.run_server(debug=True)
