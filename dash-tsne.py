import json
from pathlib import Path
import dash
import dash_cytoscape as cyto
import dash_html_components as html

elements = json.loads(Path("notebooks", "data", "network-tsne.json").read_text())

app = dash.Dash(__name__)

app.layout = html.Div(
    [
        cyto.Cytoscape(
            id="cytoscape-tsne",
            layout={"name": "preset"},
            style={"width": "100%", "height": "800px"},
            stylesheet=[
                {
                    "selector": "node",
                    "style": {
                        "label": "data(label)",
                        # "opacity": 0.7,
                        "background-color": "data(color)",
                    },
                },
                {
                    "selector": "edge",
                    "style": {
                        # "label": "data(label)",
                        "opacity": "data(opacity)",
                    },
                },
            ],
            elements=elements,
        )
    ]
)

if __name__ == "__main__":
    app.run_server(debug=True)
