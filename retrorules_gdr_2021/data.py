import os
import json
from typing import List
from pathlib import Path
from abc import ABC, abstractmethod
import hashlib
import pandas as pd
from rdkit import Chem
from matchms.importing import load_from_mgf
from retrorules_gdr_2021.logger import logger

DATA_ROOT_PATH = "DATA_ROOT_PATH"


class NoDataException(Exception):
    pass


class BaseData(ABC):
    df: pd.DataFrame = None

    def __init__(self, df: pd.DataFrame, file_key_data=None):
        self.df = df
        self.file_key_data = file_key_data

    @classmethod
    def load_from_saved_file(cls, file_key_data):
        temp_data = cls(df=None, file_key_data=file_key_data)
        file_path = temp_data.get_file_path()
        logger.debug("file_path %s", file_path)
        if file_path.exists():
            df = pd.read_csv(file_path)
            return cls(df=df, file_key_data=file_key_data)
        raise NoDataException

    def file_key(self):
        key_data_str = json.dumps(self.file_key_data).encode()
        return hashlib.md5(key_data_str).hexdigest()

    def data_key(self):
        df = self.df.reset_index(drop=True)
        return hashlib.md5(df.to_json().encode()).hexdigest()

    def save_to_file(self):
        file_path = self.get_file_path()
        logger.debug("save to file: %s", file_path)
        self.df.to_csv(file_path, index=False)

    def get_file_path(self):
        data_root_path = os.getenv(DATA_ROOT_PATH, Path())
        file_name = f"{self.save_prefix()}-{self.file_key()}.csv"
        return Path(data_root_path, file_name)

    @abstractmethod
    def save_prefix(self):
        pass


class SubstrateData(BaseData):
    def save_prefix(self):
        return "substrate_data"

    @classmethod
    def load_smiles(cls, smiles: List[str], scans: List[str] = None):
        data = {"substrate_id": range(len(smiles)), "substrate_smiles": smiles}
        if scans:
            data["substrate_scans"] = scans
        df = pd.DataFrame(data)
        df["substrate_inchikey"] = df.substrate_smiles.apply(cls.get_inchikey)
        return cls(df=df)

    @staticmethod
    def get_inchikey(smiles):
        return Chem.MolToInchiKey(Chem.MolFromSmiles(smiles))


class ExperimentalData:
    def __init__(self, data_path):
        self.spectra = list(load_from_mgf(data_path))
        self.spectra_dict = {
            spectrum.metadata["scans"]: spectrum for spectrum in self.spectra
        }
        self.metadata = pd.DataFrame(
            [
                {**spectrum.metadata, "pepmass": spectrum.metadata.pop("pepmass")[0]}
                for spectrum in self.spectra
            ]
        )


class ExportResults:

    EXPORT_KEYS = (
        "rule_id",
        "substrate_id",
        "product_inchikeys",
        "product_inchis",
        "product_smiles",
    )

    def __init__(self, folder_path="."):
        self.folder_path = folder_path

    def export(self, data, file_name):

        export = [
            {key: value for key, value in result.items() if key in self.EXPORT_KEYS}
            for result in data
        ]
        (Path(self.folder_path) / file_name).write_text(json.dumps(export))
