from pathlib import Path
import pebble
import pandas as pd
from cfm_id import CfmId
from retrorules_gdr_2021.config import config, CFM_ID_PATH, DATA_DIR

# from retrorules_gdr_2021.config import settings


class Fragmenter:
    def __init__(self, match_data: pd.DataFrame):
        self.match_data = match_data

    def gen_all_frag(self):
        # df = pd.DataFrame(self.match_data)
        # df.apply(self.gen_frag, axis=1)

        with pebble.ProcessPool(max_workers=6) as pool:
            tasks = []
            for _, row in self.match_data.iterrows():
                tasks.append(pool.schedule(gen_frag, args=(row,)))
            [task.result() for task in tasks]


def gen_frag(row):
    smiles = row.product_smiles
    inchi_key = row.product_inchikeys

    data_path = Path(config.get(DATA_DIR))
    path = data_path / f"{inchi_key}.cfmid"
    if not path.exists():
        cfm_id = CfmId(config.get(CFM_ID_PATH))
        res = cfm_id.predict(smiles, include_annotations=False)
        path.write_text(res)
    return path
