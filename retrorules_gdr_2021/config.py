import os


DATA_DIR = "DATA_DIR"
CFM_ID_PATH = "CFM_ID_PATH"


class ConfigManager:
    def get(self, key):
        return os.getenv(key)


config = ConfigManager()
