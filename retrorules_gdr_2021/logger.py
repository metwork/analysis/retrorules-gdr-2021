import os
import sys
import logging

LOGGER_NAME = "workflow"
LOGGER_LEVEL = "LOGGER_LEVEL"

logger = logging.getLogger(LOGGER_NAME)
logger_level = os.getenv(LOGGER_LEVEL, "INFO")
logger.setLevel(logger_level)

if not logger.handlers:
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logger_level)
    logger.addHandler(handler)

logger.debug("logger set to DEBUG")
