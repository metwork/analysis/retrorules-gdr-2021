import pandas as pd
import networkx as nx
from rdkit import Chem
import plotly.graph_objects as go
from .diameter import Diameter
from .config import (
    SCORE_STRUCTURE,
    SCAN_MS1_MATCHED,
    SCAN_MS2_MATCHED,
    SCAN_UNIQUE_MATCHED,
    ANNOTATION_COUNT_MAX,
    DIAMETER_DIFF_MAX,
)


class Step:
    def __init__(self, workflow, step_number: int, substrate_data):
        self.workflow = workflow
        self.step_number = step_number
        self.substrate_data = substrate_data
        self.input_smiles = list(substrate_data.df.substrate_smiles)
        self.input_scans = list(substrate_data.df.substrate_scans)
        self.input_inchikeys = [
            Chem.MolToInchiKey(Chem.MolFromSmiles(smiles))
            for smiles in self.input_smiles
        ]

    def compute(self, diameter_min, diameter_max):
        scores_df = pd.DataFrame()
        graph_df = pd.DataFrame()
        max_diameter = {}
        full_scans = []

        diameters = list(range(diameter_min, diameter_max + 2, 2))
        diameters.reverse()
        for diameter_value in diameters:
            diameter = self.compute_diameter(diameter_value)
            scores_df = scores_df.append(diameter.scores)
            diameter_graph_df = diameter.graph_df.copy()
            diameter_graph_df = diameter_graph_df[
                ~diameter.graph_df.scans.isin(full_scans)
            ]
            is_under_max_diameter = self.is_under_max_diameter(max_diameter)
            if not diameter_graph_df.empty:
                diameter_graph_df["is_under_max_diameter"] = diameter_graph_df.apply(
                    is_under_max_diameter, axis=1
                )
                diameter_graph_df = diameter_graph_df[
                    diameter_graph_df.is_under_max_diameter
                ]
                graph_df = graph_df.append(diameter_graph_df)
            max_diameter = (
                graph_df.loc[:, ["scans", "Diameter"]]
                .groupby("scans")
                .max()
                .to_dict(orient="index")
            )

            annotation_count = (
                graph_df.loc[:, ["scans", "product_inchikeys"]]
                .groupby("scans")
                .count()
                .reset_index()
            )
            full_scans = annotation_count[
                annotation_count.product_inchikeys > ANNOTATION_COUNT_MAX
            ].scans

        self.scores = scores_df
        self.graph_df = graph_df

    @staticmethod
    def is_under_max_diameter(max_diameter_dict):
        def func(row):
            max_diameter = max_diameter_dict.get(row.scans)
            if max_diameter:
                return (max_diameter["Diameter"] - row.Diameter) < DIAMETER_DIFF_MAX
            return True

        return func

    def compute_diameter(self, diameter):
        diameter = Diameter(step=self, diameter=diameter)
        diameter.compute()
        return diameter

    def generate_fig(self, **kwargs):
        columns = [
            "diameter",
            SCAN_MS1_MATCHED,
            SCAN_MS2_MATCHED,
            SCAN_UNIQUE_MATCHED,
        ]
        columns.reverse()
        scores = self.scores.sort_values("diameter")
        df = scores.loc[:, columns]
        df_plot = df.diff(axis=1)
        df_plot[columns[0]] = df[columns[0]]
        df_plot.diameter = df.diameter

        data = [
            go.Bar(
                x=df_plot.diameter,
                y=df_plot[y],
                name=name,
                marker={
                    "color": color,
                    # "opacity": 0.75,
                    "line": {"width": 0},
                },
            )
            for y, name, color in [
                (SCAN_UNIQUE_MATCHED, "Features with unique ms2 match", "#73bf73"),
                (SCAN_MS2_MATCHED, "Features with multiple ms2 match", "#bf9739"),
                (SCAN_MS1_MATCHED, "Features with only ms1 match", "#5377a6"),
            ]
        ]
        line = go.Scatter(
            x=scores.diameter,
            y=scores[SCORE_STRUCTURE],
            yaxis="y2",
            name="Molecules Generated",
            marker={"color": "#bf5543", "size": 15, "symbol": "circle"},
            line={"color": "#bf5543", "width": 6},
        )

        data.append(line)
        layout = go.Layout(
            barmode="stack",
            plot_bgcolor="white",
            yaxis={"gridcolor": "grey"},
            yaxis2={
                "position": 1,
                "overlaying": "y",
                "anchor": "y3",
                "side": "right",
                "tickfont": {"size": 16},
            },
        )
        fig = go.Figure(data=data, layout=layout)
        fig.update_layout(
            xaxis_title="Diameter (substrate specificity)",
            yaxis_title="Features matching",
            yaxis2_title="Molecules generated",
            legend={
                "yanchor": "top",
                "y": 0.99,
                "xanchor": "right",
                "x": 1.002,
            },
            font={"size": 16},
        )
        return fig

    def plot_scans(self):
        fig = self.generate_fig()
        fig.show()

    def export_scans(self, path, **kwargs):
        fig = self.generate_fig()
        fig.write_image(path, **kwargs)

    def format_graph_df(self):
        df = self.graph_df
        columns = list(set(df.columns) - {"Diameter"})
        df = df.groupby(columns).max().reset_index()
        self.graph_df = df

    def create_graph(self):
        self.format_graph_df()
        self.set_graph()

    def set_graph(self):

        df_edge = self.graph_df.loc[
            :,
            [
                "substrate_scans",
                "substrate_inchikey",
                "product_inchikeys",
                "scans",
                "Diameter"
                # "Reaction_EC_number",
            ],
        ].drop_duplicates()
        df_edge["opacity"] = (df_edge.Diameter - 8) / 8
        df_edge["source"] = df_edge.substrate_inchikey + ":" + df_edge.substrate_scans
        df_edge["target"] = df_edge.product_inchikeys + ":" + df_edge.scans
        G = nx.convert_matrix.from_pandas_edgelist(
            df_edge,
            source="source",
            target="target",
            edge_attr=["opacity", "Diameter"],
            # create_using=nx.MultiDiGraph(),
        )
        nx.set_node_attributes(G, "#c89111", "color")
        attrs = {
            f"{row.substrate_inchikey}:{row.substrate_scans}": {"color": "green"}
            for _, row in self.substrate_data.df.iterrows()
        }
        nx.set_node_attributes(G, attrs)
        attrs = {
            f"{row.product_inchikeys}:{row.scans}": {
                "cosine": str(round(row.cosine, 3))
            }
            for _, row in self.graph_df.iterrows()
        }
        nx.set_node_attributes(G, attrs)
        self.df_edge = df_edge
        self.graph = G
