import pandas as pd
from retrorules_gdr_2021.rule_burner import RuleBurner
from retrorules_gdr_2021.matcher import ResultMatcher
from retrorules_gdr_2021.frag import Fragmenter
from retrorules_gdr_2021.logger import logger
from .config import (
    SCORE_RULE,
    SCORE_STRUCTURE,
    SCORE_MS1,
    SCORE_MS2,
    SCAN_MS1_MATCHED,
    SCAN_MS2_MATCHED,
    SCAN_UNIQUE_MATCHED,
)


class Diameter:

    scores = None
    score = None

    def __init__(self, step, diameter: int):
        self.step = step
        self.diameter = diameter

    def compute(self):

        diameter = self.diameter
        logger.info(f"\n### compute for diameter {diameter}\n")
        self.score = {"diameter": diameter}

        self.compute_rule_burner(int(diameter))
        self.set_new_structures()
        self.set_ms1_match()
        self.set_ms2_match()

        self.scores = pd.Series(self.score, name=self.diameter)
        self.create_graph_df()

    def compute_rule_burner(self, diameter: int):
        self.rule_burner = RuleBurner(rule_data=self.step.workflow.rule_data)
        self.rule_burner.compute(self.step.input_smiles, diameter)

        self.set_scores(SCORE_RULE, self.rule_burner.output.df)

    def set_new_structures(self):

        df = self.rule_burner.output.df
        df = df[~df.product_inchikeys.isin(self.step.input_inchikeys)]
        self.set_scores(SCORE_STRUCTURE, df)

    def set_ms1_match(self):

        self.matcher = ResultMatcher(
            self.rule_burner.output, self.step.workflow.exp_data
        )
        self.set_match_data()
        self.set_scores(SCORE_MS1, self.match_data)

        match_count = self.matcher.match_data.groupby("scans").count()
        self.set_scores(SCAN_MS1_MATCHED, match_count)

    def set_match_data(self):
        match_data = self.matcher.match_data
        # match_data = match_data[~match_data.scans.isin(self.input_scans)]
        self.match_data = match_data

    def set_ms2_match(self):
        Fragmenter(self.match_data).gen_all_frag()
        self.matcher.add_cosine()
        self.set_match_data()
        self.ms2_match = self.match_data[
            self.match_data.cosine > self.step.workflow.cosine_threshold
        ]
        self.set_scores(SCORE_MS2, self.ms2_match)

        match_count = self.ms2_match.groupby("scans").count()
        self.set_scores(SCAN_MS2_MATCHED, match_count)

        self.set_scores(
            SCAN_UNIQUE_MATCHED, match_count.query("product_inchikeys == 1")
        )

    def set_scores(self, label, data):
        self.score[label] = len(data)
        print(f"{label} : {self.score[label]}")

    def create_graph_df(self):
        df = self.ms2_match.drop(columns="product_smiles")
        df = pd.merge(self.rule_burner.output.df, df, on="product_inchikeys")
        df = pd.merge(df, self.rule_burner.rule_data.df, on="rule_id")
        df = pd.merge(df, self.step.substrate_data.df, on="substrate_id")
        self.graph_df = df
