SCORE_RULE = "rule_results"
SCORE_STRUCTURE = "new_structures"
SCORE_MS1 = "ms1_matching"
SCORE_MS2 = "ms2_matching"
SCAN_MS1_MATCHED = "ms1_scan_matching"
SCAN_MS2_MATCHED = "ms2_scan_matching"
SCAN_UNIQUE_MATCHED = "ms2_scan_unique_matching"
ANNOTATION_COUNT_MAX = 8
DIAMETER_DIFF_MAX = 16
SIMILARITY_FACT0R = 0.8
EDGE_MATRIX_FACTOR = 0.6
