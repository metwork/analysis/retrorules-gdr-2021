import json
import numpy as np
import pandas as pd
import networkx as nx
from sklearn.manifold import TSNE
from rdkit import Chem
from rdkit import DataStructs
from retrorules_gdr_2021.data import SubstrateData
from retrorules_gdr_2021.rule_burner import RuleData
from retrorules_gdr_2021.logger import logger
from .step import Step
from .config import (
    SIMILARITY_FACT0R,
    EDGE_MATRIX_FACTOR,
)


TSNE_SPACE = 3


class Workflow:
    _rule_data = None

    def __init__(
        self, exp_data, cosine_threshold, annotation_data, min_diameter: int = 12
    ):
        self.exp_data = exp_data
        self.cosine_threshold = cosine_threshold
        self.annotation_data = annotation_data
        self.min_diameter = min_diameter

    @property
    def rule_data(self):
        if self._rule_data is None:
            self._rule_data = RuleData.create_from_file().filter_ec_digits(4)
        return self._rule_data

    def compute(self, step_count: int):
        graph = nx.Graph()
        graph_df = pd.DataFrame()
        substrate_data = self.annotation_data
        scans_set = set([])
        processed_inchi_key = set([])

        for step_number in range(step_count):
            if isinstance(self.min_diameter, int):
                min_diameter = self.min_diameter
            elif len(self.min_diameter) <= step_number:
                min_diameter = self.min_diameter[-1]
            else:
                min_diameter = self.min_diameter[step_number]
            step_number += 1
            logger.info("##### Compute step %s ", step_number)
            step = Step(
                workflow=self, step_number=step_number, substrate_data=substrate_data
            )
            step.compute(min_diameter, 16)
            step.create_graph()
            graph = nx.compose(step.graph, graph)
            graph_df = pd.concat([graph_df, step.graph_df])
            step_scans = set(step.df_edge.substrate_scans) | set(step.df_edge.scans)
            scans_set = scans_set.union(step_scans)
            processed_inchi_key = processed_inchi_key.union(
                set(step.df_edge.substrate_inchikey)
            )

            df = step.graph_df.loc[:, ["product_smiles", "product_inchikeys", "scans"]]
            df = df[~df.product_inchikeys.isin(processed_inchi_key)]
            df = df.drop_duplicates().sort_values(["product_inchikeys", "scans"])
            next_smiles = list(df.product_smiles)
            next_scans = list(df.scans)
            logger.debug("%s next smiles", len(next_smiles))
            if next_smiles:
                substrate_data = SubstrateData.load_smiles(
                    smiles=next_smiles, scans=next_scans
                )
            else:
                break

        self.graph_df = graph_df.drop_duplicates()
        self.graph = graph
        self.scans_list = list(scans_set)

    def export_graph(self, path):
        self.set_tsne_df()
        self.set_tsne_matrix()
        self.compute_tsne()
        self.set_graph_position()

        G = self.graph
        nodes = [
            {
                "data": {
                    "id": node,
                    "parent": node.split(":")[1],
                    "color": data["color"],
                    "cosine": data.get("cosine"),
                },
                "position": data["position"],
            }
            for node, data in G.nodes(data=True)
        ]
        edges = [
            {
                "data": {
                    "source": source,
                    "target": target,
                    "opacity": data["opacity"],
                    "label": str(data["Diameter"]),
                }
            }
            for source, target, data in G.edges(data=True)
        ]

        scan_mass = (
            self.exp_data.metadata.loc[:, ["scans", "pepmass"]]
            .set_index("scans")
            .to_dict(orient="index")
        )
        scans = [
            {"data": {"id": scan, "label": str(round(scan_mass[scan]["pepmass"], 3))}}
            for scan in self.scans_list
        ]
        elements = {"elements": nodes + edges + scans}
        path.write_text(json.dumps(elements))

    def plot_graph(self):
        layout = nx.spring_layout(self.graph)
        nx.draw(
            self.graph,
            with_labels=False,
            node_size=10,
            node_color="skyblue",
            pos=layout,
        )

    def set_tsne_df(self):
        df_product = self.graph_df.loc[
            :, ["product_smiles", "product_inchikeys", "scans"]
        ].rename(columns={"product_smiles": "smiles", "product_inchikeys": "inchikey"})

        df_substrate = self.graph_df.loc[
            :, ["substrate_smiles", "substrate_inchikey", "substrate_scans"]
        ].rename(
            columns={
                "substrate_smiles": "smiles",
                "substrate_scans": "scans",
                "substrate_inchikey": "inchikey",
            }
        )

        df = pd.concat(
            [df_product, df_substrate],
            ignore_index=True,
        )
        self.tsne_df = df.drop_duplicates().reset_index(drop=True)

    def set_tsne_matrix(self):
        df = self.tsne_df.copy()
        inchikeys = df.inchikey
        smiles = df.smiles
        scans = df.scans.unique()
        mols = [Chem.MolFromSmiles(sm) for sm in smiles]
        fps = [Chem.RDKFingerprint(mol) for mol in mols]

        scans_matrix = np.empty([len(smiles), len(scans)])
        for i, row in df.iterrows():
            for j, scan in enumerate(scans):
                scans_matrix[i][j] = row.scans == scan
        similarity_matrix = np.empty([len(smiles), len(smiles)])
        for i, fp1 in enumerate(fps):
            for j, fp2 in enumerate(fps):
                similarity_matrix[i][j] = DataStructs.FingerprintSimilarity(fp1, fp2)
        edge_matrix = np.empty([len(smiles), len(smiles)])
        for i, key1 in enumerate(inchikeys):
            for j, key2 in enumerate(inchikeys):
                edge_matrix[i][j] = self.edge_diameter(key1, key2)
        self.edge_matrix = edge_matrix
        self.tsne_matrix = np.concatenate(
            [
                scans_matrix,
                similarity_matrix * SIMILARITY_FACT0R,
                edge_matrix * EDGE_MATRIX_FACTOR,
            ],
            axis=1,
        )

    def edge_diameter(self, key1, key2):
        if key1 == key2:
            return 1
        df = self.graph_df.copy()
        query_string = " | ".join(
            [
                f"(substrate_inchikey == '{key1}' & product_inchikeys == '{key2}')",
                f"(substrate_inchikey == '{key2}' & product_inchikeys == '{key1}')",
            ]
        )
        df = df.query(query_string)
        if df.empty:
            return 0
        return df.Diameter.max() / 16

    def compute_tsne(self):
        tsne = TSNE(
            n_components=2,
            verbose=1,
            n_iter=250000,
            perplexity=5,
            learning_rate=1000,
            init="random",
            method="exact",
        )
        self.tsne_results = tsne.fit_transform(self.tsne_matrix) / TSNE_SPACE

    def set_graph_position(self):
        attrs = {
            f"{row.inchikey}:{row.scans}": {
                "position": {
                    "x": float(self.tsne_results[i][0]),
                    "y": float(self.tsne_results[i][1]),
                }
            }
            for i, row in self.tsne_df.iterrows()
        }
        nx.set_node_attributes(self.graph, attrs)
