import os
from typing import Dict, Union, List, Generator
import json
import hashlib
import rdkit
from rdkit.Chem.Descriptors import ExactMolWt
from retrorules_gdr_2021.logger import logger
from retrorules_gdr_2021.data import BaseData, NoDataException
import pandas as pd
import rpreactor


RULES_PATH = "RULES_PATH"
RPREACTOR_COMPUTE_TYPE = Dict[str, Union[str, List[str], List[rdkit.Chem.rdchem.Mol]]]
REACTION_EC_LABEL = "Reaction_EC_number"
NUM_WORKERS = "NUM_WORKERS"


class RuleData(BaseData):
    def save_prefix(self):
        return "rule_data_input"

    @classmethod
    def create_from_file(cls, data_path: str = None):
        if data_path is None:
            data_path = os.getenv(RULES_PATH)
            if not data_path:
                raise Exception("Missing {RULES_PATH} env variable")
        logger.debug("load rules from %s", data_path)
        df = pd.read_csv(data_path, sep="\t")
        return cls(df=df)

    def set_rule_id(self):
        df = self.df.copy()
        df["rule_id"] = range(len(df))
        return self.__class__(df)

    def filter_ec_digits(self, digits: int):
        """
        Args:
            digits: number of EC digits required.
            for example if digits=3, filter for row with EC like 1.2.3 or 1.2.3.4
        """
        df = self.df
        regex = r"^(?:\d+\.?)" + f"{{{digits},4}}"
        df_ = df.fillna("")
        df = df[df_[REACTION_EC_LABEL].str.match(regex)]

        return self.__class__(df)

    def filter_diameter(self, diameter: int):
        df = self.df.query(f"Diameter == {diameter}")
        return self.__class__(df)

    def head(self, size: int = 10):
        df = self.df.head(size)
        return self.__class__(df)


class RuleBurnerOutput(BaseData):

    INPUT_PRODUCT_TYPES = {
        "product_list": rdkit.Chem.rdchem.Mol,
        "product_inchikeys": str,
        "product_inchis": str,
        "product_smiles": str,
    }
    MASS_LABEL = "product_exact_mass"

    def save_prefix(self):
        return "rule_burner_output"

    @classmethod
    def load_compute_result(cls, compute_result, file_key_data):
        df = pd.DataFrame(compute_result)
        if not df.empty:
            for _ in range(2):
                columns = list(cls.INPUT_PRODUCT_TYPES.keys())
                df = df.explode(columns)
            df[cls.MASS_LABEL] = df.product_list.apply(cls.calculate_mass)
            df = df.drop(columns="product_list")
        df = df.astype({"rule_id": "int64", "substrate_id": "int64"})
        return cls(df=df, file_key_data=file_key_data)

    @staticmethod
    def calculate_mass(product: rdkit.Chem.rdchem.Mol):
        return ExactMolWt(product)


class RuleBurner:

    rule_data: RuleData
    output: RuleBurnerOutput = None

    def __init__(self, rule_data: RuleData = None, max_workers: int = 1):
        if rule_data is None:
            logger.debug("generate rule data")
            rule_data = RuleData.create_from_file()
        self._rule_data = rule_data

    def compute(self, smiles: List[str], diameter: int):

        self.rule_data = self._rule_data.filter_diameter(diameter).set_rule_id()
        file_key_data = {
            "smiles": smiles,
            "rule_data": self.rule_data.data_key(),
        }
        try:
            output = RuleBurnerOutput.load_from_saved_file(file_key_data=file_key_data)
            logger.debug("Data loaded from saved file")
        except NoDataException:
            logger.debug("Compute RuleBurner")
            compute = self._compute(smiles=smiles)
            output = RuleBurnerOutput.load_compute_result(
                compute, file_key_data=file_key_data
            )
            output.save_to_file()
        self.output = output

    def _compute(
        self, smiles: List[str]
    ) -> Generator[RPREACTOR_COMPUTE_TYPE, None, None]:
        rb = rpreactor.RuleBurner(with_hs=True)

        logger.debug("get rsmarts")
        rsmarts = self.rule_data.df.Rule_SMARTS.values
        if len(rsmarts) == 0:
            raise Exception("No smarts to compute")
        logger.debug("insert smarts")
        rb.insert_rsmarts(rsmarts)
        logger.debug("insert smiles")
        rb.insert_smiles(smiles)

        num_workers = int(os.getenv(NUM_WORKERS, 1))
        logger.debug("max_workers: %s", num_workers)
        return rb.compute("*", max_workers=num_workers)
