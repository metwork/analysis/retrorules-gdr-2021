from pathlib import Path
import numpy as np
import pandas as pd
from matchms.filtering import add_precursor_mz
from matchms_tools.spectrum_set import SpectrumSet
from retrorules_gdr_2021.data import ExperimentalData
from retrorules_gdr_2021.rule_burner import RuleBurnerOutput


class ResultMatcher:

    PROTON_MASS = 1.0072765
    match_data = None

    def __init__(self, rule_data: RuleBurnerOutput, exp_data: ExperimentalData):
        self.rule_data = rule_data
        self.exp_data = exp_data
        self.set_match_data()

    def set_match_data(self):
        exp_metadata = self.exp_data.metadata
        diff = np.subtract.outer(
            self.rule_data.df[RuleBurnerOutput.MASS_LABEL].values,
            exp_metadata.pepmass.values,
        )
        diff = np.abs(diff)
        diff = np.abs(diff - self.PROTON_MASS)
        diff_match = np.where(diff < 0.005)
        # diff_match = np.vstack(diff_match_0).T

        rule_data_match = self.rule_data.df.iloc[diff_match[0]]
        inchikeys = rule_data_match.product_inchikeys.reset_index()
        smiles = rule_data_match.product_smiles.reset_index()

        exp_data_match = exp_metadata.iloc[diff_match[1]]
        scans = exp_data_match.scans.reset_index()

        df = (
            pd.concat([inchikeys, smiles, scans], axis=1)
            .loc[:, ["product_inchikeys", "product_smiles", "scans"]]
            .drop_duplicates()
        )
        self.match_data = df

    def add_cosine(self):
        self.match_data["cosine"] = self.match_data.apply(self.cosine_sim, axis=1)

    def cosine_sim(self, row):
        try:
            exp_id = str(row.scans)
            sim_inchikey = row.product_inchikeys
            exp_spectrum = self.exp_data.spectra_dict[exp_id]
            cfm_spectrum = SpectrumSet.load_from_cfm_id(
                Path("data", f"{sim_inchikey}.cfmid")
            )["energy1"]
            metadata = cfm_spectrum.metadata
            metadata["pepmass"] = exp_spectrum.metadata["pepmass"]
            cfm_spectrum.metadata = metadata
            exp_spectrum = add_precursor_mz(exp_spectrum)
            cfm_spectrum = add_precursor_mz(cfm_spectrum)
            cfm_spectra = SpectrumSet(exp=exp_spectrum, sim=cfm_spectrum)
            return cfm_spectra.cosine()[0][1][0]
        except:
            return None
